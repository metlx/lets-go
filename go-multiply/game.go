package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

func main() {
	counter := 1
	for {
		scanner := bufio.NewScanner(os.Stdin)
		rand2 := randNum()
		rand1 := randNum()
		fmt.Printf("%v. %v x %v: ", counter, rand1, rand2)
		scanner.Scan()
		ans, _ := strconv.ParseInt(scanner.Text(), 10, 64)
		var prod int64
		prod = rand1 * rand2
		if ans == prod {
			fmt.Println("correct!")
			counter++
			continue
		} else if ans != prod {
			fmt.Println("kek you loose, looser!")
			break
		}
	}
}

func randNum() int64 {
	rand.Seed(time.Now().UnixNano())
	randNum1 := rand.Intn(13)
	return int64(randNum1)
}
