package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Printf("what is the name of the contact? ")
	scanner.Scan()
	noteName := scanner.Text()
	f, err := os.Create(noteName)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("enter contact information: ")
	scanner.Scan()
	test := scanner.Text()
	l, err := f.WriteString(test)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(l, "bytes written.")
	err = f.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
}
