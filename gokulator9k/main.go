package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func main() {
	for {
		is := []string{}
		scanner := bufio.NewScanner(os.Stdin)
		fmt.Printf("(help, exit or clear)> ")
		scanner.Scan()
		i := scanner.Text()
		if i == "exit" {
			break
		} else if i == "help" {
			fmt.Println("syntax: (int operand int)")
			continue
		} else if i == "" {
			continue
		} else if i == "clear" {
			cmd := exec.Command("clear")
			cmd.Stdout = os.Stdout
			cmd.Run()
			continue
		}
		is = strings.Split(i, " ")
		kek, err := strconv.Atoi(is[0])
		if is[0] == i {
			fmt.Println(err)
			continue
		}
		if err != nil {
			fmt.Println(err)
			continue
		}
		kekw, err := strconv.Atoi(is[2])
		if err != nil {
			fmt.Println(err)
			continue
		} else if is[0] == "0" || is[2] == "0" {
			fmt.Println("can't divide by zero")
			continue
		}
		switch {
		case is[1] == "-":
			fmt.Printf("%s - %s = %v \n", is[0], is[2], kek-kekw)
		case is[1] == "+":
			fmt.Printf("%s + %s = %v \n", is[0], is[2], kek+kekw)
		case is[1] == "/":
			fmt.Printf("%s / %s = %v \n", is[0], is[2], kek/kekw)
		case is[1] == "*":
			fmt.Printf("%s * %s = %v \n", is[0], is[2], kek*kekw)
		}
	}
}
