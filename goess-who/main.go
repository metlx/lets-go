package main

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/TwinProduction/go-color"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	k := 0
	for {
		rN := rand.Intn(10)
		fmt.Printf("enter a number from 0 to 9: ")
		fmt.Scanln(&k)
		if rN == k {
			fmt.Println(color.Ize(color.Blue, "CORRECT!"))
			break
		} else if rN != k {
			fmt.Println(color.Ize(color.Red, "WRONG!"))
			continue
		}
	}
}
