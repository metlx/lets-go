package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	k := ""
	for {
		rN := rand.Intn(7)
		if rN == 0 {
			rN += 1
		}
		fmt.Printf("press enter to roll, or anything else to quit. ")
		fmt.Scanln(&k)
		fmt.Printf("you roll a %v. \n", rN)
		if k != "" {
			break
		} else if k == "" {
			continue
		}
	}
}
