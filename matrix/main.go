package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
	"time"
)

func main() {
	clear := exec.Command("clear")
	clear.Stdout = os.Stdout
	clear.Run()
	neo := "\033[;32mWake up, Neo..."
	splited := strings.Split(neo, "")
	for i := 0; i < len(splited); i++ {
		time.Sleep(300 * time.Millisecond)
		fmt.Printf(splited[i])
	}
	time.Sleep(3000 * time.Millisecond)
	clear2 := exec.Command("clear")
	clear2.Stdout = os.Stdout
	clear2.Run()
	neo2 := "The Matrix has you..."
	splited2 := strings.Split(neo2, "")
	for i := 0; i < len(splited2); i++ {
		time.Sleep(500 * time.Millisecond)
		fmt.Printf(splited2[i])
	}
	time.Sleep(3000 * time.Millisecond)
	clear3 := exec.Command("clear")
	clear3.Stdout = os.Stdout
	clear3.Run()
	neo3 := "Knock, knock, Neo..."
	splited3 := strings.Split(neo3, "")
	for i := 0; i < len(splited3); i++ {
		time.Sleep(300 * time.Millisecond)
		fmt.Printf(splited3[i])
	}
	time.Sleep(3000 * time.Millisecond)
	clear4 := exec.Command("clear")
	clear4.Stdout = os.Stdout
	clear4.Run()
}
